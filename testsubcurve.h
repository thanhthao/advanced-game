
#ifndef SUBCLASS_H
#define SUBCLASS_H

#include <parametrics/gmpcurve>

class TestSubCurve : public GMlib::PCurve<float,3>{
    GM_SCENEOBJECT(TestSubCurve)

 protected:

      GMlib::PCurve<float,3>*     _curve;
      float                       _start;
      float                       _t;
      float                       _end;
      GMlib::Vector<float,3>      _trans;

      // virtual functions from PSurf
      void                    eval( float t, int d, bool l = false );
      float                   getEndP();
      float                   getStartP();

 private:

      // Local help functions
      void set(GMlib::PCurve<float,3>* curve, float start, float end, float t);


 public:
       TestSubCurve( GMlib::PCurve<float,3>* curve, float start, float end);
       TestSubCurve( GMlib::PCurve<float,3>* curve, float start, float end, float t);
       TestSubCurve(const TestSubCurve &copy );

       ~TestSubCurve();

       // virtual functions from PSurf
      bool            isClosed() const;

};

#endif
