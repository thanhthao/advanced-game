#include "blendingspline.h"
#include <cmath>

TestBlendingSpline::TestBlendingSpline(GMlib::PCurve<float,3> *b, int n){
    this->_n = n;
    _knotVector.setDim(n+2);
    _start = b->getParStart();
    _end   = b->getParEnd();
    this->createKnotVector( _start,_end);
    this->creatLocalCurve(b);
   // _sub = new TestSubCurve(b,b->getParStart(),M_PI);


}


  void TestBlendingSpline::creatLocalCurve(GMlib::PCurve<float,3> *b){
      _sub.setDim(_n);
      for(int i= 0; i< _n;i++)
          _sub[i]= new TestSubCurve(b,_knotVector[i],_knotVector[i+2],_knotVector[i+1]);

  }
TestBlendingSpline::~TestBlendingSpline(){

}
int TestBlendingSpline::findIndex(){



}
void TestBlendingSpline::eval(float t, int d, bool){



}

float TestBlendingSpline::getStartP(){

    return _start;
}


float TestBlendingSpline::getEndP(){

    return _end;
}

bool TestBlendingSpline::isClosed() const
{
  return false;
}

void TestBlendingSpline::createKnotVector(float start, float end){
    float delta= (end - start)/(_n-1);
    _knotVector[0]= _knotVector[1] = _start;

    for(int i = 1;i < _n-1; i++ )
        _knotVector[i+1]= _knotVector[i]+ delta;

    _knotVector[_n]=_knotVector[_n+1]= end;


    std::cout<<"knot vector in blending spline: "<<_knotVector<<std::endl;
    std::cout.flush();
}

