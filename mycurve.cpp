#include "mycurve.h"
#include <cmath>

MyCurve::MyCurve(){
    this->_dm   = GMlib::GM_DERIVATION_EXPLICIT;

}

MyCurve::~MyCurve(){

}

void MyCurve::eval(float t, int d, bool){

//formula http://mathhelpforum.com/trigonometry/111979-parametric-equations-graphs.html
    this->_p.setDim( d + 1 );
    this->_p[0][0] = (sin(8*t)+cos(8*t))*sin(t);
    this->_p[0][1] = (sin(8*t)+cos(8*t))*cos(t);

    this->_p[0][2] = float(0);

    if( d > 0 ) {

        this->_p[1][0] = 8*cos(8*t)*sin(t)+sin(8*t)*cos(t)-8*sin(8*t)*sin(t)+cos(8*t)*cos(t);

        this->_p[1][1] = 8*cos(8*t)*cos(t)-sin(8*t)*sin(t)-8*sin(8*t)*cos(t)-cos(8*t)*sin(t);

        this->_p[1][2] = float(0);
    }

    if( d > 1 ) {

        this->_p[2][0] = -65*sin(8*t)*sin(t)+16*cos(8*t)*cos(t)-65*cos(8*t)*sin(t)-16*sin(8*t)*cos(t);

        this->_p[2][1] = -65*sin(8*t)*cos(t)-16*cos(8*t)*sin(t)-65*cos(8*t)*cos(t)-16*sin(8*t)*sin(t);


        this->_p[2][2] = float(0);
    }
}

float MyCurve::getStartP(){

    return 0.0;

}


float MyCurve::getEndP(){

    return 2*M_PI;
}

bool  MyCurve::isClosed() const{

    return true;
}




/*
  void MyCurve::eval(float t, int d, bool){

//formula from http://mathhelpforum.com/trigonometry/111979-parametric-equations-graphs.html

    this->_p.setDim( d + 1 );
    this->_p[0][0] = (1+sin(4*t))*sin(t);
    this->_p[0][1] = (1+sin(4*t))*cos(t);

    this->_p[0][2] = float(0);

    if( d > 0 ) {

        this->_p[1][0] = cos(t)+4*cos(4*t)*sin(t)+sin(4*t)*cos(t);

        this->_p[1][1] = -sin(t) + 4*cos(4*t)*cos(t)- sin(4*t)*sin(t);

        this->_p[1][2] = float(0);
    }

    if( d > 1 ) {

        this->_p[2][0] = -sin(t) -17*sin(4*t)*sin(t) + 8*cos(4*t)*cos(t);

        this->_p[2][1] = -cos(t) -17*sin(4*t)*cos(t) - 8*cos(4*t)*sin(t);


        this->_p[2][2] = float(0);

    }
}*/
