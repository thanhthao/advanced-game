#include "testerbs.h"
#include <cmath>

TestERBS::TestERBS(GMlib::PCurve<float,3> *b, int n){

    _isBezier = false;
    this->_n = n;
    _knotVector.setDim(n+2);
    _start = b->getParStart();
    _end   = b->getParEnd();
    _closed = b->isClosed();
    this->createKnotVector(_start,_end);
    this->createLocalSubCurve(b);


}

TestERBS::TestERBS(GMlib::PCurve<float,3> *b, int n, int d){


    _isBezier = true;
    this->_n = n;
    _knotVector.setDim(n + 2);
    _start = b->getParStart();
    _end   = b->getParEnd();
    _closed = b->isClosed();
    this->createKnotVector(_start,_end);
    this->createLocalBezierCurve(b,d);
}

void TestERBS::createLocalSubCurve(GMlib::PCurve<float,3> *b){
    _curveVector.setDim(_n);
    for(int i = 1 ; i < _n ; i++)
        _curveVector[i-1]= new TestSubCurve(b,_knotVector[i-1],_knotVector[i+1],_knotVector[i]);

    if(isClosed())
        _curveVector[_n-1]= _curveVector[0];

    else
        _curveVector[_n-1]= new TestSubCurve(b,_knotVector[_n-1],_knotVector[_n+1],_knotVector[_n]);

}

void TestERBS::createLocalBezierCurve(GMlib::PCurve<float,3> *b,int d){  // d: degree or number of derivative ???

    _curveVector.setDim(_n);

    for(int i = 1; i < _n ; i++){
        _curveVector[i-1]= new TestBezier(b,_knotVector[i-1],_knotVector[i+1],_knotVector[i],d);

    }
    if(isClosed())
        _curveVector[_n-1]= _curveVector[0];

    else{
        _curveVector[_n-1]= new TestBezier(b,_knotVector[_n-1],_knotVector[_n+1],_knotVector[_n],d);

    }

}

void TestERBS::createKnotVector(float start, float end){
    float delta = (end - start)/(_n-1);  // co n kots => n-1 segments,delta: length of 1 segment
    _knotVector[0] = _knotVector[1] = _start;

    for(int i = 1 ; i < _n-1 ; i++ )
        _knotVector[i+1] = _knotVector[i]+ delta;

    _knotVector[_n] = _knotVector[_n+1] = end;


    // move 2 sides out when it is closed

    if(isClosed()){

        _knotVector[0]    = _knotVector[1] -  (_knotVector[_n] - _knotVector[_n-1]);
        _knotVector[_n+1] = _knotVector[_n] + (_knotVector[2] - _knotVector[1]);

    }


}



TestERBS::TestERBS(const TestERBS &copy){

    _knotVector   = copy._knotVector;
    _curveVector  = copy._curveVector;
    _start        = copy._start;
    _end          = copy._end;
    _n            = copy._n;

}

TestERBS::~TestERBS(){

}

int TestERBS::findIndex(float t){

    // ex: 0 0 1 2 3 4 4 :start from second 0 to the first 4.
    //because the first 0:_knotVector[i-1]
    //the last 4: no _knotVector[i+1]
    for(int i = 1; i < _knotVector.getDim()-2; i++)
        if(t >= _knotVector[i] && t < _knotVector[i+1]  )
            return i;

    return _knotVector.getDim() - 3; // the last satisfy the condition
}

float  TestERBS::getMap(float t,int index ){

    if(_isBezier)

        return (t-_knotVector[index-1])/(_knotVector[index + 1] - _knotVector[index-1]);
    else
        return t;

}

void TestERBS::eval(float t, int d, bool){ // t: parameter value, d: number of derivative to compute

    _p.setDim(d+1);

    int   index = findIndex(t);
    float w     = (t - _knotVector[index])/(_knotVector[index + 1] - _knotVector[index]);
    float scale = 1.0;

    GMlib::DVector<float> B = BFunction(w,d,scale);

    GMlib::DVector< GMlib::Vector<float,3>>  c1 = _curveVector[index-1]->evaluateParent(getMap(t,index),d) ;

    GMlib::DVector< GMlib::Vector<float,3>>  c2 = _curveVector[index]->evaluateParent(getMap(t,index+1),d) ;

    _p[0] = c1[0]+ B[0]*( c2[0] -  c1[0]);

    if( d > 0)
        _p[1] = c1[1]+ B[0]*( c2[1] -  c1[1])
                + B[1]*( c2[0] -  c1[0]) ;  // the first derivative of _p[0]

    if( d > 1)
        _p[2] = c1[2]+ B[0]*( c2[2] -  c1[2])
                + 2*B[1]*( c2[1] -  c1[1])
                + B[2]*( c2[0] -  c1[0]) ; // the second derivative of _p[0]

}

GMlib::DVector<float> TestERBS::BFunction(float t, int d, float scale){ //d :degree, scale: from 0 to 1

    GMlib::DVector<float> b(d+1);
    //test d to calculate derivative saved in  b[1],b[2]....
    // b[0] : formula
    b[0] = 3*std::pow(t,2) - 2*std::pow(t,3);

    if(d > 0)
        b[1] = (6*t-6*std::pow(t,2))*scale ;
    if (d > 1)
        b[2] =(6-12*t )*scale;
    if(d > 2)
        b[3] = -12 * scale* scale;

    return b;

}
void TestERBS::localSimulate(double dt){

    for(int i = 0; i < _curveVector.getDim(); i++){
        if( i%2 == 0)
            _curveVector[i]->rotate(dt, GMlib::Point<float,3>( 0.0f, 0.0f, 1.0f ) );
        else
            _curveVector[i]->rotate(dt, GMlib::Point<float,3>( 1.0f, 0.0f, 0.0f ) );
    }



    this->replot();

}

float TestERBS::getStartP(){

    return _knotVector[1];
}


float TestERBS::getEndP(){

    return _knotVector[_knotVector.getDim()-2];
}

bool TestERBS::isClosed() const
{
    return _closed;
}

