#include "controller.h"

#include <QDebug>

Controller::Controller(){
    _update = 0.0;
    _moveRight = true;

    this->toggleVisible();
    insertScene();
}

Controller::~Controller(){
    delete    _bezier;
    delete    _sub;
    delete    _mycurve;
    delete    _erbs;
    delete    _erbs2;
    delete    _erbs3;
    delete    _erbs4;
    delete    _erbs5;
    delete    _erbs6;

}

void Controller::localSimulate(double dt){

    if(_moveRight){
        _update += dt;
        _erbs->translate(GMlib::Vector<float,3>( 0.5f, 0.0f, 0.0f )*dt);
        _erbs2->translate(GMlib::Vector<float,3>( -0.5f, 0.0f, 0.0f )*dt);
        _erbs3->translate(GMlib::Vector<float,3>( 0.0f, -0.5f, 0.0f )*dt);
        _erbs4->translate(GMlib::Vector<float,3>( 0.0f, 0.5f, 0.0f )*dt);


    }
    else{
        _update -= dt;
        _erbs->translate(GMlib::Vector<float,3>( -0.5f, 0.0f, 0.0f )*dt);
        _erbs2->translate(GMlib::Vector<float,3>( 0.5f, 0.0f, 0.0f )*dt);
        _erbs3->translate(GMlib::Vector<float,3>( 0.0f, 0.5f, 0.0f )*dt);
        _erbs4->translate(GMlib::Vector<float,3>( 0.0f, -0.5f, 0.0f )*dt);


    }
    if(_update > 10.0)
        _moveRight = false;
    if(_update < -10.0)
        _moveRight = true;

    _erbs5->rotate(10,GMlib::Point<float,3>( 0.0f, 0.0f, 1.0f ));
    _erbs6->rotate(10,GMlib::Point<float,3>( 0.0f, 0.0f, 1.0f ));

}

void Controller::insertScene(){

    _mycurve = new MyCurve();
    _mycurve->toggleDefaultVisualizer();
    _mycurve->replot(1000,2);
    this->insert(_mycurve);

    //        GMlib::DVector<GMlib::Vector<float,3> > a1 = _mycurve->evaluate(_mycurve->getParStart(),1); // 1: first derivative
    //        GMlib::DVector<GMlib::Vector<float,3> > a2 = _mycurve->evaluate(_mycurve->getParEnd(),1);
    //        std::cout<<"my curve: "<<std::endl;
    //        std::cout<<"start: "<<a1<<std::endl; //if start = end => curve is closed
    //        std::cout<<"end: "<<a2<<std::endl;


    //    _bezier = new TestBezier(_mycurve,0.0,0.01,0.0,3);
    //    _bezier->toggleDefaultVisualizer();
    //    _bezier->replot(100,1);
    //    _bezier->setColor(GMlib::GMcolor::Beige);
    //    _bezier->translate(GMlib::Vector<float,3>( 0.0f, 0.1f, 0.1f ));
    //    this->insert( _bezier);


    //     _sub = new TestSubCurve(_mycurve,_mycurve->getParStart(),50.0);  //a part of my curve
    //     _sub->toggleDefaultVisualizer();
    //     _sub->replot(1000,2);
    //     this->insert( _sub);


    _erbs = new TestERBS(_mycurve,160);
    _erbs->toggleDefaultVisualizer();
    _erbs->replot(1000,1);
    _erbs->setColor(GMlib::GMcolor::YellowGreen);
    _erbs->translate(GMlib::Vector<float,3>( -3.0f, 0.0f, 0.0f ));
    this->insert(_erbs);


    _erbs2 = new TestERBS(_mycurve,160,2);
    _erbs2->toggleDefaultVisualizer();
    _erbs2->replot(1000,1);
    _erbs2->setColor(GMlib::GMcolor::Blue);
    _erbs2->translate(GMlib::Vector<float,3>( 3.0f, 0.0f, 0.0f ));
    this->insert(_erbs2);

    _erbs3 = new TestERBS(_mycurve,160,2);
    _erbs3->toggleDefaultVisualizer();
    _erbs3->replot(1000,1);
    _erbs3->setColor(GMlib::GMcolor::Bisque);
    _erbs3->translate(GMlib::Vector<float,3>( 0.0f, 3.0f, 0.0f ));
    this->insert(_erbs3);

    _erbs4 = new TestERBS(_mycurve,160,2);
    _erbs4->toggleDefaultVisualizer();
    _erbs4->replot(1000,1);
    _erbs4->setColor(GMlib::GMcolor::Violet);
    _erbs4->translate(GMlib::Vector<float,3>( 0.0f, -3.0f, 0.0f ));
    this->insert(_erbs4);

    _erbs5 = new TestERBS(_mycurve,160);
    _erbs5->toggleDefaultVisualizer();
    _erbs5->replot(1000,1);
    _erbs5->setColor(GMlib::GMcolor::Black);
    _erbs5->translate(GMlib::Vector<float,3>( 6.0f, 6.0f, 0.0f ));
    this->insert(_erbs5);

    _erbs6 = new TestERBS(_mycurve,160);
    _erbs6->toggleDefaultVisualizer();
    _erbs6->replot(1000,1);
    _erbs6->setColor(GMlib::GMcolor::Black);
    _erbs6->translate(GMlib::Vector<float,3>( -6.0f, 6.0f, 0.0f ));
    this->insert(_erbs6);


    this->setSurroundingSphere(GMlib::Sphere<float,3>(100));

}

