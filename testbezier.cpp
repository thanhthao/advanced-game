#include "testbezier.h"
#include <cmath>

TestBezier::TestBezier(GMlib::PCurve<float,3>* curve,float start, float end,float t, int d){
    _closed = curve->isClosed();
    _curve  = curve;
    _start  = start;
    _end    = end;
    _t      = t ;
    _d      = d;

   this->ControlPoint(t,d);


}

TestBezier::~TestBezier(){

     delete _curve;
}

void TestBezier::ControlPoint(float t, int d){

    float scale   = 1/(_end - _start);
    float t_scale = (t - _start)/(_end - _start);

    GMlib::DMatrix<float> m = this->BezierHermiteMatrix(d,t_scale,scale).invert();

    GMlib::DVector<GMlib::Vector<float,3> > g = _curve->evaluateParent(t,d);
     _controlpoint = m * g;

    for(int i= 0; i<_controlpoint.getDim(); i++)
        _controlpoint[i] -= g[0];
    this->translate(g[0]);



}



void TestBezier::eval(float t, int d, bool){

    this->_p.setDim(1 + _d);
    float scale = 1/(_end - _start);
    GMlib::DMatrix<float> n = BezierHermiteMatrix(_d,t,scale);
    this->_p = n * _controlpoint;
    this->_p.setDim(1 + d); //???
}

float TestBezier::getStartP(){
   return 0.0;
   }


float TestBezier::getEndP(){
   return 1.0;

}

GMlib::DMatrix<float> TestBezier::BezierHermiteMatrix(int d, float t, float scale ){
    GMlib::DMatrix<float> B(d+1,d+1);

    B[d-1][0]= 1 - t;
    B[d-1][1]= t;

    for(int i = d - 2; i >= 0; i--){
        B[i][0] = (1-t) * B[i+1][0];
        for(int j = 1 ; j < d-i ; j++)
            B[i][j] = t * B[i+1][j-1]+(1-t) * B[i+1][j] ;
        B[i][d-i] = t * B[i+1][d-i-1];

    }

    B[d][0] = -scale;
    B[d][1] = scale;

    for(int k = 2; k <= d ; k++){

        double s = k * scale;
        for(int i = d; i> d-k ; i--){
            B[i][k] = s * B[i][k-1] ;

            for(int j = k-1; j>0 ; j--)
                B[i][j] = s *(B[i][j-1] - B[i][j] );

            B[i][0] = -s * B[i][0];
        }
    }

   return B;
}

bool TestBezier::isClosed(){
    return _closed;
}
