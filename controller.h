#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "testsubcurve.h"
#include "testbezier.h"
#include "testerbs.h"
#include "mycurve.h"
#include <gmParametricsModule>

// GMlib
#include <core/gmarray>
#include <core/gmdmatrix>
#include <parametrics/gmpbutterfly>
// Qt
#include <QDebug>
#include <stdexcept>
#include <iostream>

class Controller:public GMlib::SceneObject{
    GM_SCENEOBJECT(Controller)
private:
     TestBezier*                _bezier;
     TestSubCurve*              _sub;
     MyCurve *                  _mycurve;
     TestERBS*                  _erbs;
     TestERBS*                  _erbs2;
     TestERBS*                  _erbs3;
     TestERBS*                  _erbs4;
     TestERBS*                  _erbs5;
     TestERBS*                  _erbs6;

     bool                       _moveRight;
     float                      _update;

    GMlib::DVector<GMlib::PCurve<float,3>* > _curveVector;
public:

    Controller();
    ~Controller();
    void insertScene();
protected:

    void localSimulate(double dt);   //virtual function, inherit from SceneObject

};


#endif // CONTROLLER_H


