#include "testsubcurve.h"

  TestSubCurve::TestSubCurve(GMlib::PCurve<float,3>* curve, float start, float end )
  {
    this->_dm = GMlib::GM_DERIVATION_EXPLICIT; // _dm derivative method

    set(curve, start, end, (end + start)/2);

    GMlib::DVector<GMlib::Vector<float,3> > tr = _curve->evaluateParent(_t, 0);
    _trans = tr[0];
    this->translateParent( _trans );


  }



  TestSubCurve::TestSubCurve(GMlib::PCurve<float,3>* curve, float start, float end, float t )
  {
    this->_dm = GMlib::GM_DERIVATION_EXPLICIT;

    set(curve, start, end, t);

     GMlib::DVector< GMlib::Vector<float,3> > tr = _curve->evaluateParent(t, 0);

     _trans = tr[0];
    this->translateParent( _trans );

  }



  TestSubCurve::TestSubCurve( const TestSubCurve& copy ) : GMlib::PCurve<float,3>( copy )
  {
    set(copy._curve, copy._start, copy._end, copy._t);

    _trans = copy._trans;
  }



  TestSubCurve::~TestSubCurve() {}


   void TestSubCurve::eval( float t, int d, bool /*l*/ )
  {
    this->_p     = _curve->evaluateParent(t , d);
    this->_p[0] -= _trans;
  }



  float TestSubCurve::getStartP()
  {
    return _start;
  }



  float TestSubCurve::getEndP()
  {
    return _end;
  }



  bool TestSubCurve::isClosed() const
  {
    return false;
  }


 void TestSubCurve::set(GMlib::PCurve<float,3>* curve, float start, float end, float t)
  {
    _curve = curve;
    _start = start;
    _t = t;
    _end = end;
  }


