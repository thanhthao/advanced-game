
#ifndef ERBSCLASS_H
#define ERBSCLASS_H



#include "testsubcurve.h"
#include "testbezier.h"
#include "testerbs.h"
#include <gmParametricsModule>

class TestERBS : public GMlib::PCurve<float,3>{
    GM_SCENEOBJECT(TestERBS)

private:

    GMlib::DVector<float>                           _knotVector;
    GMlib::DVector<GMlib::PCurve<float,3>* >        _curveVector;
    float                                           _start;
    float                                           _end;
    int                                             _n;
    bool                                            _closed;
    bool                                            _isBezier;

    GMlib::DVector<float> BFunction(float t,int d,float scale);
    float  getMap(float t, int index);
public:

    TestERBS(GMlib::PCurve<float,3> *b,int n);        // for subcurve, n : number of local curve
    TestERBS(GMlib::PCurve<float,3> *b,int n,int d);  // for Bezier curve, d : derivative
    TestERBS(const TestERBS &copy);
    ~TestERBS();


    bool  isClosed() const;
    int   findIndex(float t);

protected:

    float getStartP();
    float getEndP();
    void  eval(float t, int d, bool l = true ) ;
    void localSimulate(double dt);

    void createKnotVector(float start, float end);
    void createLocalSubCurve(GMlib::PCurve<float,3> *b);
    void createLocalBezierCurve(GMlib::PCurve<float,3> *b, int d);


};


#endif
