
#ifndef MYCURVECLASS_H
#define MYCURVECLASS_H

#include <parametrics/gmpcurve>

class MyCurve : public GMlib::PCurve<float,3>{
    GM_SCENEOBJECT(MyCurve)

public:
    MyCurve();
    ~MyCurve();
    bool   isClosed() const;
protected:

    float getStartP();
    float getEndP();
    void  eval(float t, int d, bool l = true ) ;


};

#endif
