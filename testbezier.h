
#ifndef BEZIERCLASS_H
#define BEZIERCLASS_H

#include <gmParametricsModule>

class TestBezier : public GMlib::PCurve<float,3>{
    GM_SCENEOBJECT(TestBezier)

private:


public:

    TestBezier(GMlib::PCurve<float,3>* curve, float start, float end, float t, int d); // t ???
    ~TestBezier();
    bool isClosed();

protected:
   GMlib::DVector<GMlib::Vector<float,3> > _controlpoint;
   GMlib::PCurve<float,3>*                 _curve;
    float                                  _start;
    float                                  _t;
    float                                  _end;
    bool                                   _closed;
    int                                    _d;


    float getStartP();
    float getEndP();
    void  eval(float t, int d, bool l = true ) ;


    GMlib::DMatrix<float> BezierHermiteMatrix( int d, float t, float scale );
    void ControlPoint(float t, int d);

};

#endif
