
#ifndef BLENDINGCLASS_H
#define BLENDINGCLASS_H

//#include <parametrics/gmpsurf>

#include "testsubcurve.h"
#include "testerbs.h"
#include <parametrics/gmpbutterfly>
#include <parametrics/gmpcurve>

class TestBlendingSpline : public GMlib::PCurve<float,3>{
    GM_SCENEOBJECT(TestBlendingSpline)

 private:
       GMlib::DVector<float>                 _knotVector;
       GMlib::DVector<TestSubCurve *>        _sub;
       float                                 _start;
       float                                 _end;
       int                                   _n;

 public:
    TestBlendingSpline(GMlib::PCurve<float,3> * b,int n);
    ~TestBlendingSpline();

    bool  isClosed() const;
    int   findIndex();
protected:

    float getStartP();
    float getEndP();
    void  eval(float t, int d, bool l = true ) ;


    void createKnotVector(float start, float end);
    void creatLocalCurve(GMlib::PCurve<float,3> * b);


};

#endif
